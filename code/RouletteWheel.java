import java.util.Random;

public class RouletteWheel {
    //Create a private field of type Random 
    private Random random;
    //Create a private field of type int to store the number of the last spin
    private int lastSpin;
    //Create a method spin().
    public void spin() {
        random = new Random();
        lastSpin = random.nextInt(37);
    }
    //Create a method getValue(). 
    public int getValue() {
        return lastSpin;
    }
}
