import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        //Create a RouletteWheel object
        RouletteWheel rouletteWheel = new RouletteWheel();
        //Every user starts out with 1000$.
        int startMoney = 1000;
        //Ask the user whether they’d like to make a bet and if so, what number they’d like to bet on.
        while(true){
            System.out.println("You have $" + startMoney + ". Would you like to make a bet? (y/n)");
            Scanner input = new Scanner(System.in);
            String answer = input.nextLine();
            if (answer.equals("y")) {
                System.out.println("What number would you like to bet on?");
                Scanner input1 = new Scanner(System.in);
                int betNumber = input1.nextInt();
                System.out.println("How much would you like to bet?");
                Scanner input2 = new Scanner(System.in);
                int betAmount = input2.nextInt();
                //Check if betAmount is smaller or equal to the amount of money the user has.
                if (betAmount > startMoney) {
                    System.out.println("You don't have enough money to make this bet. You have $" + startMoney + ". Please try again.");
                    continue;
                }
                //If the user wins, they get 35 times the amount they bet. If they lose, they lose the amount they bet.
                rouletteWheel.spin();
                if (rouletteWheel.getValue() == betNumber) {
                    startMoney += betAmount * 35;
                    System.out.println("The number is: " + rouletteWheel.getValue() +". You won $" + betAmount * 35 + "! You have $" + startMoney + " left.");
                } 
                else {
                    startMoney -= betAmount;
                    System.out.println("The number is: " + rouletteWheel.getValue() +". You lost $" + betAmount + ". You have $" + startMoney + " left.");
                }
            }
            else if (answer.equals("n")) {
                if (startMoney > 1000) {
                    System.out.println("You won $" + (startMoney - 1000) + ". You have $" + startMoney + ". Thanks for playing!");
                }
                else if (startMoney < 1000) {
                    System.out.println("You lost $" + (1000 - startMoney) + ". You have $" + startMoney + ". Thanks for playing!");
                }
                else
                System.out.println("You still have $" + startMoney + ". Thanks for playing!");
                break;
            }
            else {
                System.out.println("Invalid input. Please try again.");
            }
        }
    }
}